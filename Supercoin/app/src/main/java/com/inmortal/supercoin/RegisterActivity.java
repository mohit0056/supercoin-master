package com.inmortal.supercoin;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import com.inmortal.supercoin.utill.ApiURL;
import com.inmortal.supercoin.utill.NetworkCall;
import com.inmortal.supercoin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.shobhitpuri.custombuttons.GoogleSignInButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends AppCompatActivity implements NetworkCall.MyNetworkCallBack {
    CallbackManager callbackManager;
    private static final String TAG = "RegisterActivity";
    private GoogleSignInButton signInButton;
    private GoogleApiClient googleApiClient;
    LoginButton loginButton;
    private static final int RC_SIGN_IN = 1;
    String name, email, userid, myemail, myphone, myIdToken;
    RelativeLayout fb;
    Boolean cancel;
    static final String pref_name = "supercoin";
    String idToken, mynumber, mypassword;
    FirebaseUser user;
    Boolean isLogin;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    SweetAlertDialog sweetAlertDialog;
    Progress progress;
    EditText number, password;
    SharedPreferences mSharedPreference;
    NetworkCall networkCall;
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
    Button btnmobileRegister;

    @Override
    protected void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_register);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        isLogin = mSharedPreference.getBoolean("login", false);


        if (isLogin) {
            Intent intent = new Intent(RegisterActivity.this, SelectAndEarn.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        firebaseAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));
        myphone = (mSharedPreference.getString("mobile", ""));
        myemail = (mSharedPreference.getString("myemail", ""));
        myIdToken = (mSharedPreference.getString("token", ""));
        progress = new Progress(RegisterActivity.this);
        networkCall = new NetworkCall(RegisterActivity.this, RegisterActivity.this);

        number = (EditText) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);

        btnmobileRegister = (Button) findViewById(R.id.register);
        btnmobileRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mynumber = number.getText().toString().trim();
                mypassword = password.getText().toString().trim();


                number.setError(null);
                password.setError(null);


                if (TextUtils.isEmpty(mynumber)) {
                    number.setError("Phone number is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mypassword)) {
                    password.setError("Password is required");
                    cancel = true;
                } else if (!mynumber.matches(regexStr)) {
                    number.setError("Please Enter Valid Phone no.");
                    cancel = true;
                } else if (mypassword.length() < 6) {
                    password.setError("Password minimum contain 6 character");
                    cancel = true;
                } else {

                    signUpMobile();


                }
            }
        });

//        isLogin = mSharedPreference.getBoolean("login", false);
//        if (isLogin) {
//            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finish();
//        }

        //this is where we start the Auth state Listener to listen for whether the user is signed in or not
//        authStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                // Get signedIn user
//                user = firebaseAuth.getCurrentUser();
//
//                //if user is signed in, we call a helper method to save the user details to Firebase
//                if (firebaseAuth.getCurrentUser() != null) {
//
//                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(i);
//                    // User is signed in
//                    // you could place other firebase code
//                    //logic to save the user details to Firebase
//                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                } else {
//                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
//                }
//            }
//        };
//
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.web_client_id))//you can also use R.string.default_web_client_id
//                .requestEmail()
//                .build();
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//
////        signInButton = findViewById(R.id.sign_in_button);
////        fb = findViewById(R.id.fbbtn);
////        signInButton.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
////                startActivityForResult(intent, RC_SIGN_IN);
////            }
////        });
//
//
//        callbackManager = CallbackManager.Factory.create();
//
//        loginButton = findViewById(R.id.loginbutton);
//
//        //Setting the permission that we need to read
////        loginButton.setReadPermissions("public_profile", "email", "user_birthday");

        //Registering callback!
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                //Sign in completed
//                Log.i(TAG, "onSuccess: logged in successfully");
//
//                //handling the token for Firebase Auth
//                handleFacebookAccessToken(loginResult.getAccessToken());
//
//                //Getting the user information
//                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                        // Application code
//                        Log.i(TAG, "onCompleted: response: " + response.toString());
//                        try {
//                            String email = object.getString("email");
//                            String birthday = object.getString("birthday");
//
//                            Log.i(TAG, "onCompleted: Email: " + email);
//                            Log.i(TAG, "onCompleted: Birthday: " + birthday);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.i(TAG, "onCompleted: JSON exception");
//                        }
//                    }
//                });
//
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,email,gender,birthday");
//                request.setParameters(parameters);
//                request.executeAsync();
//
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d(TAG, "facebook:onCancel");
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d(TAG, "facebook:onError", error);
//            }
//        });
//
//    }
//
//    private void handleFacebookAccessToken(AccessToken token) {
//        Log.d(TAG, "handleFacebookAccessToken:" + token);
//
//        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
//        firebaseAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            // Sign in success, update UI with the signed-in user's information
//                            Log.d(TAG, "signInWithCredential:success");
//                            FirebaseUser user = firebaseAuth.getCurrentUser();
//                            Log.i(TAG, "onComplete: login completed with user: " + user.getDisplayName());
//
//                        } else {
//                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//                        // ...
//                    }
//                });
    }
//

    private void signUpMobile() {
        networkCall.NetworkAPICall(ApiURL.register, true);

    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        }
//    }
//
//    private void handleSignInResult(GoogleSignInResult result) {
//        if (result.isSuccess()) {
//            GoogleSignInAccount account = result.getSignInAccount();
//            idToken = account.getIdToken();
//            name = account.getDisplayName();
//            email = account.getEmail();
//            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//            SharedPreferences.Editor editor = prefs.edit();
//
//            editor.putString("myemail", email);
//            editor.putString("token", idToken);
//
//            editor.apply();
//
//            // you can store user data to SharedPreference
//            AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
//            firebaseAuthWithGoogle(credential);
//        } else {
//            // Google Sign In failed, update UI appropriately
//            Log.e(TAG, "Login Unsuccessful. " + result);
//            Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    private void firebaseAuthWithGoogle(AuthCredential credential) {
//
//        firebaseAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
//                        if (task.isSuccessful()) {
//                            Toast.makeText(RegisterActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
//                            MainActivity();
//                        } else {
//                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
//                            task.getException().printStackTrace();
//                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                });
//    }


//    private void MainActivity() {
//        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
//    }
//
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (authStateListener != null) {
//            firebaseAuth.removeAuthStateListener(authStateListener);
//        }
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
//    public void onclick(View view) {
//        if (view == fb) {
//            loginButton.performClick();
//        }
//    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.register:
                ion = (Builders.Any.B) Ion.with(RegisterActivity.this)
                        .load("POST", ApiURL.register)
                        .setBodyParameter("mobile", mynumber)
//                        .setBodyParameter("email", myemail)
                        .setBodyParameter("password", mypassword);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.register:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");
                    if (succes.equals("true")) {
                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("id");
                            myphone = Jasonobject1.getString("mobile");
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("id", userid);
                        editor.putString("mobile", myphone);
                        editor.apply();
                        mSharedPreference.edit().putBoolean("login", true).apply();
                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Super coin");
                        sweetAlertDialog.setContentText(msg);
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(RegisterActivity.this, R.color.red));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent intent = new Intent(RegisterActivity.this, SelectAndEarn.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();
                    } else {
                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("SuperCoin")
                                    .setContentText(msg)
                                    .show();

//                            Toast.makeText(Registration.this, msg.toString(), Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }

                    }
                } catch (JSONException e1) {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("SuperCoin")
                            .setContentText("Oops... Something went wrong!")
                            .show();

                    //Toast.makeText(Registration.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("SuperCoin")
                .setContentText(jsonstring)
                .show();
    }
}