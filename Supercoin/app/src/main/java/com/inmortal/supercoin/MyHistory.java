package com.inmortal.supercoin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.supercoin.utill.ApiURL;
import com.inmortal.supercoin.utill.NetworkCall;
import com.inmortal.supercoin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyHistory extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter_listing.ReturnView {
    Handler handler;
    private RecyclerView listRecycler;
    ImageView back_btn;
    SharedPreferences mSharedPreference;
    ArrayList<Model_Listing> arrModelListing = new ArrayList<>();
    Adapter_listing adapter_listing;
    Model_Listing model_listing;
    SweetAlertDialog sweetAlertDialog;
    NetworkCall networkCall;
    String numId, colorId, userid, Bet;
    Progress progress;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_history);

        progress = new Progress(MyHistory.this);
        networkCall = new NetworkCall(MyHistory.this, MyHistory.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(MyHistory.this);

        colorId = (mSharedPreference.getString("color_id", ""));
        numId = (mSharedPreference.getString("num_id", ""));
        userid = (mSharedPreference.getString("id", ""));

        listRecycler = (RecyclerView) findViewById(R.id.listing);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyHistory.this,SelectAndEarn.class);
                startActivity(intent);
                MyHistory.super.onBackPressed();
                finish();
            }
        });

        bitListng();
    }

    private void bitListng() {

        networkCall.NetworkAPICall(ApiURL.bid_listing, true);

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {


        Builders.Any.B ion = null;
        switch (apitype) {


            case ApiURL.bid_listing:
                ion = (Builders.Any.B) Ion.with(MyHistory.this)
                        .load("POST", ApiURL.bid_listing)
                        .setBodyParameter("user_id", userid);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {



            case ApiURL.bid_listing:
                try {
                    JSONObject bidListing = new JSONObject(jsonstring.toString());
                    String status = bidListing.getString("success");
                    if (status.equals("true")) {

                        JSONArray withdrawHistoryList = bidListing.getJSONArray("data");
                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_listing = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_Listing.class);
                            arrModelListing.add(model_listing);

                        }

                        adapter_listing = new Adapter_listing(arrModelListing, MyHistory.this, R.layout.list_model, this, 1);

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MyHistory.this);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);

                        listRecycler.setLayoutManager(linearLayoutManager);
                        listRecycler.setHasFixedSize(true);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);
                        listRecycler.setNestedScrollingEnabled(false);
                        listRecycler.setAdapter(adapter_listing);

                    } else {

                        String status_fail = bidListing.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(MyHistory.this, "Data not found", Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(MyHistory.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getbidList(View view, List objects, int position, int from) {
        Model_Listing model_listing = arrModelListing.get(position);


        TextView period = view.findViewById(R.id.period);

        TextView price = view.findViewById(R.id.price);

        TextView number = view.findViewById(R.id.number);

        TextView color = view.findViewById(R.id.joincolor);
        TextView adminColor = view.findViewById(R.id.adminColor);
        TextView adminNum = view.findViewById(R.id.adminNum);
        TextView winningPrice = view.findViewById(R.id.winningAmount);


        String myPeriod = String.valueOf(model_listing.getBid_id());
        String myBidAmount = model_listing.getUser_amount();
        String myBidNumber = model_listing.getBidnumber_id();
        String myColor = model_listing.getColor_id();
        String myAdminNumb = model_listing.getAdmin_no();
        String myAdminColor = model_listing.getAdmin_color();
        String myWinningPrice = model_listing.getWin_price();

        period.setText(myPeriod);
        price.setText(myBidAmount);

        if (myBidNumber!=null){

            number.setText(myBidNumber);
        }else
        {
            number.setText("NJ");
        }
        if(myColor!=null){

            color.setText(myColor);

        }else{

            color.setText("NJ");
        }


        adminColor.setText(myAdminColor);
        adminNum.setText(myAdminNumb);
        winningPrice.setText(myWinningPrice);


    }
}