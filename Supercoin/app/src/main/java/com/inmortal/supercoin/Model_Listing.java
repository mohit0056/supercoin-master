package com.inmortal.supercoin;

public class Model_Listing {

    private int id;

    private String bid_id;

    private String user_id;

    private String color_id;

    private String bidnumber_id;

    private String user_amount;

    private String admin_no;

    private String admin_color;

    private String win_price;

    private String created_at;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setBid_id(String bid_id){
        this.bid_id = bid_id;
    }
    public String getBid_id(){
        return this.bid_id;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setColor_id(String color_id){
        this.color_id = color_id;
    }
    public String getColor_id(){
        return this.color_id;
    }
    public void setBidnumber_id(String bidnumber_id){
        this.bidnumber_id = bidnumber_id;
    }
    public String getBidnumber_id(){
        return this.bidnumber_id;
    }
    public void setUser_amount(String user_amount){
        this.user_amount = user_amount;
    }
    public String getUser_amount(){
        return this.user_amount;
    }
    public void setAdmin_no(String admin_no){
        this.admin_no = admin_no;
    }
    public String getAdmin_no(){
        return this.admin_no;
    }
    public void setAdmin_color(String admin_color){
        this.admin_color = admin_color;
    }
    public String getAdmin_color(){
        return this.admin_color;
    }
    public void setWin_price(String win_price){
        this.win_price = win_price;
    }
    public String getWin_price(){
        return this.win_price;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}
