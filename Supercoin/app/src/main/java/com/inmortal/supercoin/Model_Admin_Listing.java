package com.inmortal.supercoin;

public class Model_Admin_Listing {
    private String bid_id;

    private String admin_color;

    private String admin_no;

    public void setBid_id(String bid_id){
        this.bid_id = bid_id;
    }
    public String getBid_id(){
        return this.bid_id;
    }
    public void setAdmin_color(String admin_color){
        this.admin_color = admin_color;
    }
    public String getAdmin_color(){
        return this.admin_color;
    }
    public void setAdmin_no(String admin_no){
        this.admin_no = admin_no;
    }
    public String getAdmin_no(){
        return this.admin_no;
    }
}
