package com.inmortal.supercoin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.google.gson.JsonObject;
import com.inmortal.supercoin.utill.ApiURL;
import com.inmortal.supercoin.utill.MyDialog;
import com.inmortal.supercoin.utill.NetworkCall;
import com.inmortal.supercoin.utill.Progress;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class Withdrawal extends AppCompatActivity implements NetworkCall.MyNetworkCallBack{
    ImageView back_btn;
   Button withdrawBTN;
    Progress progress;
    Boolean cancel;
    SweetAlertDialog sweetAlertDialog;
    EditText holdertxt,banktxt,ifsctxt,accounttxt,mobiletxt,amounttxt;
    NetworkCall networkCall;
     SharedPreferences mSharedPreference;
   String userid,myholdername,myamount,mybank,myifsc,mymobile,myaccountnum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);

        progress = new Progress(Withdrawal.this);
        networkCall = new NetworkCall(Withdrawal.this, Withdrawal.this);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userid = (mSharedPreference.getString("id", ""));


        back_btn = (ImageView) findViewById(R.id.back_btn);



holdertxt = findViewById(R.id.holderName);
accounttxt = findViewById(R.id.accountNum);
amounttxt = findViewById(R.id.amount);
banktxt = findViewById(R.id.bankName);
ifsctxt = findViewById(R.id.ifscCode);
mobiletxt = findViewById(R.id.mobileNumber);

        withdrawBTN = (Button) findViewById(R.id.withdraw);
        withdrawBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myamount = amounttxt.getText().toString().trim();
                mybank = banktxt.getText().toString().trim();
                myaccountnum = accounttxt.getText().toString().trim();
                myifsc = ifsctxt.getText().toString().trim();
                myholdername = holdertxt.getText().toString().trim();
                mymobile = mobiletxt.getText().toString().trim();


                amounttxt.setError(null);
                banktxt.setError(null);
                accounttxt.setError(null);
                ifsctxt.setError(null);
                holdertxt.setError(null);
                mobiletxt.setError(null);


                if (TextUtils.isEmpty(myamount)) {
                    amounttxt.setError("This field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myaccountnum)) {
                    accounttxt.setError("This field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mybank)) {
                    banktxt.setError("This field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myholdername)) {
                    holdertxt.setError("This field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(myifsc)) {
                    ifsctxt.setError("This field is required");
                    cancel = true;
                } else if (TextUtils.isEmpty(mymobile)) {
                    mobiletxt.setError("This field is required");
                    cancel = true;
                } else {

                    withdrawMoney();

                }
                }
            });


    }


            private void withdrawMoney() {
                networkCall.NetworkAPICall(ApiURL.withdraw_money, true);

            }

            @Override
            public Builders.Any.B getAPIB(String apitype) {
                Builders.Any.B ion = null;
                switch (apitype) {
                    case ApiURL.withdraw_money:
                        ion = (Builders.Any.B) Ion.with(Withdrawal.this)
                                .load("POST", ApiURL.withdraw_money)
                                .setBodyParameter("user_id", userid)
                                .setBodyParameter("amount", myamount)
                                .setBodyParameter("bank_name", mybank)
                                .setBodyParameter("account_number", myaccountnum)
                                .setBodyParameter("ifsc", myifsc)
                                .setBodyParameter("holder_name", myholdername)
                                .setBodyParameter("mobile", mymobile);
                        break;
                }
                return ion;
            }

            @Override
            public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
                switch (apitype) {

                    case ApiURL.withdraw_money:
                        try {
                            JSONObject jsonObject = new JSONObject(jsonstring.toString());
                            String succes = jsonObject.getString("success");
                            String msg = jsonObject.getString("message");
                            if (succes.equals("true")) {

                                sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                                sweetAlertDialog.setTitleText("Super coin");
                                sweetAlertDialog.setContentText(msg);
                                sweetAlertDialog.show();

                                Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(ContextCompat.getColor(Withdrawal.this, R.color.red));

                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent intent = new Intent(Withdrawal.this, Profile.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                sweetAlertDialog.setCancelable(false);
                                sweetAlertDialog.show();
                            } else {
                                String status_fail = jsonObject.getString("success");
                                if (status_fail.equals("false")) {
                                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("SuperCoin")
                                            .setContentText(msg)
                                            .show();

//                            Toast.makeText(Registration.this, msg.toString(), Toast.LENGTH_SHORT).show();
                                    progress.dismiss();
                                }

                            }
                        } catch (JSONException e1) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("SuperCoin")
                                    .setContentText("Oops... Something went wrong!")
                                    .show();

                            //Toast.makeText(Registration.this, jsonstring.getJSONArray("msg").toString(), Toast.LENGTH_SHORT).show();
                        }


                        break;
                }
            }

            @Override
            public void ErrorCallBack(String jsonstring, String apitype) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("SuperCoin")
                        .setContentText(jsonstring)
                        .show();
            }




        }