package com.inmortal.supercoin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.bumptech.glide.Glide;
import com.gocashfree.cashfreesdk.CFPaymentService;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.JsonObject;
import com.inmortal.supercoin.utill.ApiURL;
import com.inmortal.supercoin.utill.MyDialog;
import com.inmortal.supercoin.utill.NetworkCall;
import com.inmortal.supercoin.utill.Progress;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class Profile extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener , NetworkCall.MyNetworkCallBack{

    Button logoutBtn,AddMoneyBTN;
    TextView userName,userEmail,userId,currentAmount;
    String myemail,myMoney,userid,currentMoney,myphone;
    EditText addmoneyEdit;
    CircleImageView profileImage;
    Progress progress;
    SweetAlertDialog sweetAlertDialog;
    ImageView back_btn;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;
    static final String pref_name = "superLike";
    private GoogleApiClient googleApiClient;
    Boolean isLogin;
    int addmoney;
    private GoogleSignInOptions gso;
    Boolean cancel;
    TextView phoneNumber;

    Button addMoneybtn;

    private String txn_id = "";

    String menteramountedit="",creditedamount="",totalamount="",orderid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mSharedPreference = getSharedPreferences(pref_name, Context.MODE_PRIVATE);
        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        userid = (mSharedPreference.getString("id", ""));
        currentMoney = (mSharedPreference.getString("currentMoney", ""));
        myphone = (mSharedPreference.getString("mobile", ""));
        progress = new Progress(Profile.this);
        networkCall = new NetworkCall(Profile.this, Profile.this);

        addmoneyEdit = (EditText) findViewById(R.id.AddMoneyAmount);

        phoneNumber = (TextView) findViewById(R.id.phoneNumber);


        profileImage = findViewById(R.id.profile_image);
        currentAmount = findViewById(R.id.current_amount);

        if (currentMoney.equals("")) {
            currentAmount.setText("0");
        } else {
            currentAmount.setText(currentMoney);
        }

        if (myphone.equals("")) {
            phoneNumber.setText("0");
        } else {
            phoneNumber.setText(myphone);
        }


        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Profile.this,SelectAndEarn.class);
                startActivity(intent);
                Profile.super.onBackPressed();
                finish();
            }
        });


        AddMoneyBTN = (Button) findViewById(R.id.AddMoneyBtn);
        AddMoneyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menteramountedit=addmoneyEdit.getText().toString();

                orderid = ApiURL.getOrderID();
                double withmoneycas = Double.parseDouble(menteramountedit);
                if (withmoneycas <= 49) {
                    Toast.makeText(Profile.this,  "Amount should be greater then Rs.50", Toast.LENGTH_SHORT).show();
                }else if (menteramountedit.equals("")){
                    Toast.makeText(Profile.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();

                } else {
                    cashfree_genrate_token(orderid,addmoneyEdit.getText().toString());
                }

            }
        });

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient=new GoogleApiClient.Builder(this)
                .enableAutoManage(Profile.this,Profile.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();


//        logoutBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FirebaseAuth.getInstance().signOut();
//                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//                                if (status.isSuccess()){
//                                    gotoRegister();
//                                }else{
//                                    Toast.makeText(getApplicationContext(),"Session not close",Toast.LENGTH_LONG).show();
//                                }
//                            }
//                        });
//            }
//        });
    }

    private void addMoney() {
        networkCall.NetworkAPICall(ApiURL.add_money, true);

    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account=result.getSignInAccount();
            myemail = account.getEmail();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();

            editor.putString("Email", myemail);

            editor.apply();

            userName.setText(account.getDisplayName());
            userEmail.setText(account.getEmail());

            try{
                Glide.with(this).load(account.getPhotoUrl()).into(profileImage);
            }catch (NullPointerException e){
                Toast.makeText(getApplicationContext(),"image not found",Toast.LENGTH_LONG).show();
            }

        }else{
            //gotoRegister();
        }
    }
    private void gotoRegister(){

        Intent intent = new Intent(Profile.this,RegisterActivity.class);
        startActivity(intent);
        Profile.this.finish();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void withdraw(View view) {

        Intent intent = new Intent(Profile.this,Withdrawal.class);
        startActivity(intent);

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void cashfree_genrate_token(final String orderid, final String amount) {
        progress.show();
        JsonObject json = new JsonObject();
        json.addProperty("orderId", orderid);
        json.addProperty("orderAmount", amount);
        json.addProperty("orderCurrency", "INR");
//      json.addProperty("user_id", MyProfile.getProfile().getUserId());

        Ion.with(Profile.this)
                .load("POST", ApiURL.cashfreeurl)
                .setHeader("x-client-id", ApiURL.CASHFREE_mid)
                .setHeader("x-client-secret", ApiURL.CASHFREE_SECRETID)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        progress.dismiss();
                        try {
                            Log.d("cashfreedata", String.valueOf(result.toString()));
                            JSONObject jsonObject = new JSONObject(result.toString());
                            if (jsonObject.optString("status").equalsIgnoreCase("OK")) {
                                doPayment(jsonObject.optString("cftoken"), orderid, amount);
                            } else {
                                Toast.makeText(Profile.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException ex) {
                            progress.dismiss();
                            ex.printStackTrace();
                        } catch (NullPointerException ex) {
                            progress.dismiss();
                            Toast.makeText(Profile.this, "SERVER ERROR", Toast.LENGTH_SHORT).show();


                            ex.printStackTrace();
                        }
//                        JSONObject jsonObject = new JSONObject(result.)
                        // do stuff with the result or error
                    }
                });
    }

    public void doPayment(String Tokken, String odr_id, String amount) {

        String token = Tokken;
//       String stage = "PROD";
////        String appId = "42503bba18ea3af85ed7e9ec730524";
        String stage = "TEST";
        String appId = ApiURL.CASHFREE_mid;
        Map<String, String> params = new HashMap<>();
        String mobile_number = "";
        String email_id = "";


//        params.put(PARAM_PAYMENT_OPTION, "card");
//        params.put(PARAM_CARD_NUMBER, "4434260000000008");//Replace Card number
//        params.put(PARAM_CARD_MM, "05"); // Card Expiry Month in MM
//        params.put(PARAM_CARD_YYYY, "2021"); // Card Expiry Year in YYYY
//        params.put(PARAM_CARD_HOLDER, "John Doe"); // Card Holder name
//        params.put(PARAM_CARD_CVV, "123");


//        params.put(PARAM_PAYMENT_OPTION, "upi");
//        params.put(PARAM_UPI_VPA, "testsuccess@gocash");

//        if (Integer.parseInt(mobile) >= 10) {
//            mobile_number = mobile;
//        } else {
//            mobile_number = "+910000000000";
//        }

        params.put(PARAM_APP_ID, appId);
        params.put(PARAM_ORDER_ID, odr_id);
        params.put(PARAM_ORDER_AMOUNT, amount);
        params.put(PARAM_ORDER_NOTE, "");
        params.put(PARAM_CUSTOMER_NAME, "testingandroid");
        params.put(PARAM_CUSTOMER_PHONE, "+910000000000");
        params.put(PARAM_CUSTOMER_EMAIL, "manojchahal93@gmail.com");



//        params.put(PARAM_NOTIFY_URL, ApiUrl.NOTIFI_URL);
//        postData.put("referenceId", REFERENCE_ID);
//        for (Map.Entry entry : params.entrySet()) {
//            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
//        }

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(0);

        // Use the following method for initiating Payments
        // First color - Toolbar background
        // Second color - Toolbar text and back arrow color
        //cfPaymentService.doPayment(this, params, token, stage, "#CA272A", "#CA272A", true);
        cfPaymentService.doPayment(this, params, token, stage, "#8f3295", "#FFFFFFFF", true);

        //upiPayment(this, params,token,stage);


    }

    public  void  upiPayment(Context context, Map<String, String> params, String token, String stage){
        addamount();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("", "API Response : ");
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();

            if (bundle != null)
                Log.e("suceess_data", bundle.toString());

            for (String key : bundle.keySet()) {
                if (bundle.getString(key) != null) {
                    Log.e("TAG", key + " : " + bundle.getString(key));
                }
            }

            try {

                if (bundle.getString("txStatus").equalsIgnoreCase("Success") || bundle.getString("txMsg").equalsIgnoreCase("Transaction Successful")) {
//                  /
//                        onBackPressed();
//                    }
                    Toast.makeText(this, bundle.getString("txMsg"), Toast.LENGTH_SHORT).show();
                    txn_id=bundle.getString("referenceId");
                    updateWalletAmount(bundle.getString("orderAmount"));
//                                        add_cash_amount(bundle.getString("orderId"), bundle.getString("orderAmount"));
                } else {
//                    Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
                }
//                Utility.showToastMessage(AddCashActivity.this, bundle.getString("txMsg"), "2");
            } catch (Exception e) {
//                Utility.showToastMessage(/AddCashActivity.this, "payment cancelled ", "2");
            }
        }
    }

    private void updateWalletAmount(String orderAmount) {
        MyDialog.getInstance(Profile.this).showDialog();

        Ion.with(this)
                .load("POST", ApiURL.add_money)
                .setBodyParameter("user_id",userid)
                .setBodyParameter("txn_id",txn_id)
                .setBodyParameter("amount",orderAmount)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        MyDialog.getInstance(Profile.this).hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("true")) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                creditedamount=jsonObject1.getString("amount");
                                totalamount=jsonObject1.getString("total_amount");

                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("creditedamount", creditedamount);
                                editor.putString("totalamount", totalamount);
                                editor.apply();

                                Intent intent=new Intent(Profile.this, SelectAndEarn.class);
                                startActivity(intent);

                                finish();

                            } else if (success.equals("false")) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                progress.dismiss();
                            }

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            MyDialog.getInstance(Profile.this).hideDialog();
                        }
                    }
                });

    }

    private void addamountnew(String orderAmount, String cashfree_wallet) {

    }

    private void addamount() {
        networkCall.NetworkAPICall(ApiURL.url_add_wallet_balwahan,true);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.add_money:
                ion = (Builders.Any.B) Ion.with(getApplicationContext())
                        .load("POST", ApiURL.add_money)
                        .setBodyParameter("user_id",userid)
                        .setBodyParameter("order_id",orderid)
                        .setBodyParameter("txn_id",txn_id)
                        .setBodyParameter("amount",menteramountedit);
                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {

            case ApiURL.add_money:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    String message = jsonObject.getString("message");
                    if (success.equals("true")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        creditedamount=jsonObject1.getString("amount");
                        totalamount=jsonObject1.getString("total_amount");

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("creditedamount", creditedamount);
                        editor.putString("totalamount", totalamount);
                        editor.apply();
                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Super coin");
                        sweetAlertDialog.setContentText(message);
                        sweetAlertDialog.show();

                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(Profile.this, R.color.red));

                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                Intent intent=new Intent(Profile.this, SelectAndEarn.class);
                                startActivity(intent);

                                finish();
                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();


                    } else if (success.equals("false")) {
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("SuperCoin")
                                .setContentText(message)
                                .show();

//                            Toast.makeText(Registration.this, msg.toString(), Toast.LENGTH_SHORT).show();
                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(getApplicationContext(), jsonstring.getJSONArray("message").toString(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        Toast.makeText(getApplicationContext(),jsonstring, Toast.LENGTH_SHORT).show();

    }
}