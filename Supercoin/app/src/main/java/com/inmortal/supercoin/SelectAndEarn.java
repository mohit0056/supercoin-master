package com.inmortal.supercoin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.supercoin.utill.ApiURL;
import com.inmortal.supercoin.utill.NetworkCall;
import com.inmortal.supercoin.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SelectAndEarn extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter_Admin.ReturnView {
    Handler handler;
    private RecyclerView AdminRecycler;

    SharedPreferences mSharedPreference;
    ArrayList<Model_Admin_Listing> arrModelAdminListing = new ArrayList<>();
    Adapter_Admin adapter_admin;
    Model_Admin_Listing model_admin_listing;
    SweetAlertDialog sweetAlertDialog;
    private int defaultPostion = -1;

    NetworkCall networkCall;
    String numId, colorId, userid, Bet;
    Button Submit;
    Boolean cancel;
    EditText enterBid;
    int count =0;
    Progress progress;
    TextView textView;

    RelativeLayout joinred, joinblue, joingreen, one, two, three, four, five, six, seven, zero, eight, nine, ten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_and_earn);

        textView = findViewById(R.id.textView);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(SelectAndEarn.this);

        colorId = (mSharedPreference.getString("color_id", ""));
        numId = (mSharedPreference.getString("num_id", ""));
        userid = (mSharedPreference.getString("id", ""));

        progress = new Progress(SelectAndEarn.this);
        networkCall = new NetworkCall(SelectAndEarn.this, SelectAndEarn.this);

        countDown();
//
//        handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                countDown();
//            }
//        }, 500);



        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        countDown();                    }
                });
            }
        }, 500);



        AdminRecycler = (RecyclerView) findViewById(R.id.Adminlisting);
        enterBid = (EditText) findViewById(R.id.editTxtBid);

        Submit = findViewById(R.id.submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bet = enterBid.getText().toString().trim();
                enterBid.setError(null);
                if (TextUtils.isEmpty(Bet)) {
                    enterBid.setError("This field is required");
                    cancel = true;
                } else {
                    SubmitButton();
                }
            }
        });


        bitListng();


        joinblue = findViewById(R.id.joinBlue);
        joinblue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                colorId = "Blue";
                joinblue.setBackgroundResource(R.drawable.yellow_button_background);
                joingreen.setBackgroundResource(R.drawable.green_button_background);
                joinred.setBackgroundResource(R.drawable.button_background);

            }
        });

        joingreen = findViewById(R.id.joinGreen);
        joingreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorId = "Green";
                joinblue.setBackgroundResource(R.drawable.blue_button_background);
                joingreen.setBackgroundResource(R.drawable.yellow_button_background);
                joinred.setBackgroundResource(R.drawable.button_background);

            }
        });

        joinred = findViewById(R.id.joinRed);
        joinred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                colorId = "Red";
                joinblue.setBackgroundResource(R.drawable.blue_button_background);
                joingreen.setBackgroundResource(R.drawable.green_button_background);
                joinred.setBackgroundResource(R.drawable.yellow_button_background);
            }
        });

        zero = findViewById(R.id.zero);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "0";
                zero.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
            }
        });
        one = findViewById(R.id.one);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "1";

                one.setBackgroundResource(R.drawable.numberback);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        two = findViewById(R.id.two);
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "2";

                two.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        three = findViewById(R.id.three);
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "3";

                three.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        four = findViewById(R.id.four);
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "4";

                four.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        five = findViewById(R.id.five);
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "5";

                five.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        six = findViewById(R.id.six);
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "6";

                six.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        seven = findViewById(R.id.seven);
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "7";

                seven.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
            }
        });
        eight = findViewById(R.id.eight);
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "8";

                eight.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
                nine.setBackgroundResource(R.drawable.background_border);
            }
        });
        nine = findViewById(R.id.nine);
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numId = "9";

                nine.setBackgroundResource(R.drawable.numberback);
                one.setBackgroundResource(R.drawable.background_border);
                two.setBackgroundResource(R.drawable.background_border);
                three.setBackgroundResource(R.drawable.background_border);
                four.setBackgroundResource(R.drawable.background_border);
                five.setBackgroundResource(R.drawable.background_border);
                six.setBackgroundResource(R.drawable.background_border);
                seven.setBackgroundResource(R.drawable.background_border);
                eight.setBackgroundResource(R.drawable.background_border);
                zero.setBackgroundResource(R.drawable.background_border);
            }
        });
    }
    private void SubmitButton() {
        networkCall.NetworkAPICall(ApiURL.userbid_store, true);
    }
    private void bitListng() {
        networkCall.NetworkAPICall(ApiURL.Admin_Listing, true);
    }
     private void countDown() {
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                // Used for formatting digit to be in 2 digits only
//               count++;

                NumberFormat f = new DecimalFormat("00");
                long hour = (millisUntilFinished / 3600000) % 24;
                long min = (millisUntilFinished / 60000) % 60;
                long sec = (millisUntilFinished / 1000) % 60;
                textView.setText(f.format(hour) + ":" + f.format(min) + ":" + f.format(sec));

                refresh(60000);

            }

            // When the task is over it will print 00:00:00 there
            public void onFinish() {
                textView.setText("Finish");


            }
        }.start();
    }

    private void refresh(int miliseconds) {

        handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                countDown();

            }
        };
        handler.postDelayed(runnable, miliseconds);

    }


    @Override
    public void getAdminList(View view, List objects, int position, int from) {

        Model_Admin_Listing model_admin_listing = arrModelAdminListing.get(position);
        TextView period = view.findViewById(R.id.period);
        TextView adminColor = view.findViewById(R.id.adminColor);
        TextView adminNum = view.findViewById(R.id.adminNum);
        String myPeriod = String.valueOf(model_admin_listing.getBid_id());
        String myAdminNumb = model_admin_listing.getAdmin_no();
        String myAdminColor = model_admin_listing.getAdmin_color();
        adminColor.setText(myAdminColor);
        adminNum.setText(myAdminNumb);
        period.setText(myPeriod);

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.userbid_store:
                ion = (Builders.Any.B) Ion.with(SelectAndEarn.this)
                        .load("POST", ApiURL.userbid_store)
                        .setBodyParameter("user_id", userid)
                        .setBodyParameter("color_id", colorId)
                        .setBodyParameter("number_id", numId)
                        .setBodyParameter("bid_amount", Bet);
                break;
            case ApiURL.Admin_Listing:
                ion = (Builders.Any.B) Ion.with(SelectAndEarn.this)
                        .load("GET", ApiURL.Admin_Listing)
                       ;
                break;
        }
        return ion;

    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.userbid_store:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String succes = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");
                    if (succes.equals("true")) {
                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonObject1.length(); i++) {
                            JSONObject Jasonobject1 = jsonObject1.getJSONObject(0);
                            userid = Jasonobject1.getString("user_id");
                        }
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("id", userid);
                        editor.apply();
                        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Super coin");
                        sweetAlertDialog.setContentText(msg);
                        sweetAlertDialog.show();
                        Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(ContextCompat.getColor(SelectAndEarn.this, R.color.dark_green));
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                Intent intent = new Intent(SelectAndEarn.this, SelectAndEarn.class);
//                                startActivity(intent);
//                                finish();
                                sweetAlertDialog.dismiss();
                                bitListng();
                            }
                        });
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.show();
                    } else {
                        String status_fail = jsonObject.getString("success");
                        if (status_fail.equals("false")) {
                            sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Super coin");
                            sweetAlertDialog.setContentText(msg);
                            sweetAlertDialog.show();
                            Button btn = (Button) sweetAlertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(ContextCompat.getColor(SelectAndEarn.this, R.color.red));

                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                    Intent intent = new Intent(SelectAndEarn.this, SelectAndEarn.class);
//                                    startActivity(intent);
//                                    finish();
                                    sweetAlertDialog.dismiss();
                                }
                            });
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.show();
                        }

                    }
                } catch (JSONException e1) {
                    Toast.makeText(SelectAndEarn.this, jsonstring.getJSONArray("" + e1).toString(), Toast.LENGTH_SHORT).show();
                }

                break;
                case ApiURL.Admin_Listing:
                try {
                    arrModelAdminListing.clear();
                    JSONObject bidListing = new JSONObject(jsonstring.toString());
                    String status = bidListing.getString("success");
                    if (status.equals("true")) {

                        JSONArray withdrawHistoryList = bidListing.getJSONArray("data");
                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_admin_listing = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_Admin_Listing.class);
                            arrModelAdminListing.add(model_admin_listing);

                        }

                        adapter_admin = new Adapter_Admin(arrModelAdminListing, SelectAndEarn.this, R.layout.admin_list_model, this, 1);

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SelectAndEarn.this);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);

                        AdminRecycler.setLayoutManager(linearLayoutManager);
                        AdminRecycler.setHasFixedSize(true);
                        linearLayoutManager.setReverseLayout(true);
                        linearLayoutManager.setStackFromEnd(true);
//                        AdminRecycler.setNestedScrollingEnabled(false);
                        AdminRecycler.setAdapter(adapter_admin);

                    } else {

                        String status_fail = bidListing.getString("success");
                        if (status_fail.equals("false")) {
                            Toast.makeText(SelectAndEarn.this, "Data not found", Toast.LENGTH_SHORT).show();
                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(SelectAndEarn.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Super Like")
                .setContentText(jsonstring)
                .show();
    }

    public void MyHistory(View view) {
        Intent intent = new Intent(SelectAndEarn.this,MyHistory.class);
        startActivity(intent);
        finish();
    }

    public void profile(View view) {
        Intent intent = new Intent(SelectAndEarn.this,Profile.class);
        startActivity(intent);
        finish();

    }
}